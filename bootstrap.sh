#!/usr/bin/env bash

PHPDIR=/etc/php/7.0

echo -e "\n--- Ready to install... ---\n"
echo -e "\n--- Updating packages list ---\n"
apt-get -qq update

echo -e "\n--- Installing base packages ---\n"
apt-get -y install curl build-essential unzip > /dev/null 2>&1

echo -e "\n--- Updating packages list - Again ---\n"
apt-get -qq update

echo -e "\n--- Installing Nginx packages and config ---\n"
apt-get -y install nginx > /dev/null 2>&1
ufw allow 'Nginx HTTP' > /dev/null 2>&1

echo -e "\n--- Installing PHP ---\n"
apt-get -y install php php-fpm php-xml > /dev/null 2>&1

sed -i "s/;cgi.fix_pathinfo=1/cgi.fix_pathinfo=0/" $PHPDIR/fpm/php.ini

echo -e "\n--- Configuring servers ---\n"
systemctl restart php7.0-fpm
rm /etc/nginx/sites-available/default
cp /vagrant/nginx-config /etc/nginx/sites-available/default
# sudo nginx -t
systemctl reload nginx
sed -i 's/user = www-data/user = vagrant/g' $PHPDIR/fpm/pool.d/www.conf
sed -i 's/group = www-data/group = vagrant/g' $PHPDIR/fpm/pool.d/www.conf
# sed -i 's/;php_flag[display_errors] = off/php_flag[display_errors] = on/g' $PHPDIR/fpm/pool.d/www.conf

echo -e "\n--- Installing Composer ---\n"
cd ~
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('SHA384', 'composer-setup.php') === '544e09ee996cdf60ece3804abc52599c22b1f40f4323403c44d44fdfdd586475ca9813a858088ffbc1f233e9b180f061') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
php composer-setup.php
php -r "unlink('composer-setup.php');"
mv composer.phar /usr/local/bin/composer

echo -e "\n--- Installing project dependencies ---\n"
cd /vagrant/web
composer install --no-plugins --no-scripts > /dev/null 2>&1

echo -e "\n--- Restarting servers ---\n"
systemctl restart php7.0-fpm
systemctl reload nginx
