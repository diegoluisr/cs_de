CS.DE Test
==========
The following code is a technical test implementation.

Why Vagrant?
------------
Vagrant is a virtualization tool that allows a team to work in a homogeneous environment, trying to copy all possibles configuration of the production context to avoid or reduce any possible error or misconfiguration.

Why Slim?
---------
Slim is a microservices framework, I have experience working on it, and is pretty easy to use an configure.

Requirements
------------
In order to run this project, you require to have installed
* [Vagrant](https://www.vagrantup.com/)
* [VirtualBox](https://www.virtualbox.org/)

Setup
-----
Open your terminal and run the below commands

Clone the repository
```bash
$ git clone git@bitbucket.org:diegoluisr/cs_de.git
```
Change directory
```bash
$ cd cs_de
```
Run (vagrant and virtual box required) This command will execute all tasks related to set ready the environment. 
```bash
$ vagrant up
```
Then you could go into the virtualized machine using next command
```bash
$ vagrant ssh
```
Code Quality Testing
--------------------
I'm using PSR2 standard for code quality testing purposes
```bash
$ cd /vagrant/web
$ ./vendor/bin/phpcs --standard=PSR2 src/
$ ./vendor/bin/phpcs --standard=PSR2 public/index.php
$ ./vendor/bin/phpcs --standard=PSR2 tests/
```
Tests
-----
To test the application you could do it with UnitTest or manually
*Unit Test*
```bash
$ cd /vagrant/web
$ ./vendor/bin/phpunit ./tests/Functional/CashWithdrawTest.php
```
*Manually* using cURL (Terminal, Postman or similar)
```bash
curl -X POST \
  http://localhost:8282/api/cash/withdraw/ \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -H 'Postman-Token: a6cb3488-e3d3-4741-a1d0-2e2094656e13' \
  -d '{"ammount":"30.00"}'
```
```bash
curl -X POST \
  http://localhost:8282/api/cash/withdraw/ \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -H 'Postman-Token: a6cb3488-e3d3-4741-a1d0-2e2094656e13' \
  -d '{"ammount":"80.00"}'
```
```bash
curl -X POST \
  http://localhost:8282/api/cash/withdraw/ \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -H 'Postman-Token: a6cb3488-e3d3-4741-a1d0-2e2094656e13' \
  -d '{"ammount":"125.00"}'
```
```bash
curl -X POST \
  http://localhost:8282/api/cash/withdraw/ \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -H 'Postman-Token: a6cb3488-e3d3-4741-a1d0-2e2094656e13' \
  -d '{"ammount":"-130.00"}'
```
```bash
curl -X POST \
  http://localhost:8282/api/cash/withdraw/ \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -H 'Postman-Token: 94c55e95-4d3f-45ee-98bb-5ded90032d3f'
```
Finally
-------
Stop vagrant
```bash
$ vagrant halt
```
Destroy the virtualized machine.
```bash
$ vagrant destroy
```
