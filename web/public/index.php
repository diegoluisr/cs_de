<?php
// error_reporting(E_ALL | E_STRICT);
// ini_set('display_errors', 1);

if (PHP_SAPI == 'cli-server') {
    // To help the built-in PHP dev server, check if the request was actually for
    // something which should probably be served as a static file
    $url  = parse_url($_SERVER['REQUEST_URI']);
    $file = __DIR__ . $url['path'];
    if (is_file($file)) {
        return false;
    }
}

require __DIR__ . '/../vendor/autoload.php';

session_start();

// Instantiate the app
$settings = require __DIR__ . '/../src/settings.php';
$app = new \Slim\App($settings);

// Adding default handlers.
$container = $app->getContainer();
$container['phpErrorHandler'] = function ($container) {
    return function ($request, $response, $error) use ($container) {

        $data = [
            'error' => get_class($error),
            'file' => $error->getFile(),
            'line' => $error->getLine(),
            'trace' => [],
        ];

        foreach ($error->getTrace() as $trace) {
            $data['trace'][] = $trace;
        }

        return $container['response']
            ->withStatus(500)
            ->withJson($data);
    };
};
$container['errorHandler'] = function ($container) {
    return function ($request, $response, $exception) use ($container) {
        $data = [
            'exception' => get_class($exception),
            'message' => $exception->getMessage(),
        ];

        return $container['response']
            ->withStatus(500)
            ->withJson($data);
    };
};
$container['notFoundHandler'] = function ($container) {
    return function ($request, $response) use ($container) {
        return $container['response']
            ->withStatus(404)
            ->withJson(['message' => 'Page not found']);
    };
};

// Set up dependencies
require __DIR__ . '/../src/dependencies.php';

// Register middleware
require __DIR__ . '/../src/middleware.php';

// Register routes
require __DIR__ . '/../src/routes.php';

// Run app
$app->run();
