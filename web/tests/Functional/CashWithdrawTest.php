<?php

namespace Tests\Functional;

class CashWithdrawTest extends BaseTestCase
{

    /**
    * @dataProvider notesProvider
    */
    public function testGetNotesWithValues($data, $payload)
    {
        $response = $this->runApp('POST', '/api/cash/withdraw/', $data);

        $this->assertEquals(200, $response->getStatusCode());
        $body = (string)$response->getBody();
        $this->assertEquals($payload, $body);
    }

    public function notesProvider()
    {
        return [
            [['ammount' => '30.00'], '["20.00","10.00"]'],
            [['ammount' => '80.00'], '["50.00","20.00","10.00"]'],
        ];
    }

    public function testGetUnavailable()
    {
        $response = $this->runApp('POST', '/api/cash/withdraw/', ['ammount' => '125.00']);

        $this->assertEquals(500, $response->getStatusCode());
        $body = (string)$response->getBody();
        $this->assertContains('Core\\Exceptions\\NoteUnavailableException', $body);
    }

    public function testGetInvalidArgument()
    {
        $response = $this->runApp('POST', '/api/cash/withdraw/', ['ammount' => '-130.00']);

        $this->assertEquals(500, $response->getStatusCode());
        $body = (string)$response->getBody();
        $this->assertContains('InvalidArgumentException', $body);
    }

    public function testGetNull()
    {
        $response = $this->runApp('POST', '/api/cash/withdraw/');

        $this->assertEquals(200, $response->getStatusCode());
        $body = (string)$response->getBody();
        $this->assertEquals('[]', $body);
    }
}
