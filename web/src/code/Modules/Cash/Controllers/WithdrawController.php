<?php

namespace Modules\Cash\Controllers;

use Slim\Http\Request;
use Slim\Http\Response;
use Core\Controllers\AppController;
use Core\Exceptions\NoteUnavailableException;

/**
 * Class withdraw to calculate notes.
 */
class WithdrawController extends AppController
{

    /**
     * Contructor
     */
    public function __construct()
    {
    }

    /**
     * Overwritting postAction.
     * @param Slim\Http\Request $request
     * @param Slim\Http\Response $response
     * @param array $args
     */
    public function postAction(Request $request, Response $response, $args)
    {

        $ammount = $request->getParsedBodyParam('ammount', null);

        if (is_null($ammount)) {
            return [];
        }

        if (floatval($ammount) < 0.0) {
            throw new \InvalidArgumentException(
                'Withdraw not allowed because ammount is less than zero.'
            );
        }

        if (!$this->isDivisible($ammount)) {
            throw new NoteUnavailableException(
                'Withdraw not allowed because minimun note unavailable.'
            );
        }

        $notes = $this->getNotes();
        // usort($notes);

        $data = [];

        foreach ($notes as $note) {
            $count = intval(floatval($ammount) / floatval($note));
            if ($count > 0.0) {
                $data = array_merge($data, array_fill(0, $count, $note));
                $ammount = floatval($ammount) - (floatval($note) * $count);
            }
        }

        return $data;
    }

    /**
     * Get all enables notes.
     *
     * @return array
     */
    private function getNotes()
    {
        return ['100.00', '50.00', '20.00', '10.00'];
    }

    /**
     * Function to validate it the ammount could be divided.
     *
     * @param mixed $ammount.
     * @return bool
     */
    private function isDivisible($ammount)
    {
        $min = min($this->getNotes());
        if (intval($ammount) % intval($min) === 0) {
            return true;
        }

        return false;
    }
}
