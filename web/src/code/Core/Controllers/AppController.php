<?php

namespace Core\Controllers;

use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Base classe with default endpoints implementation, this class allow to answer properly
 * any request.
 */
class AppController
{

    protected $result = [];

    /**
     * Contructor
     */
    public function __construct()
    {
    }

    /**
     * Function to process http GET method
     * @param Slim\Http\Request $request
     * @param Slim\Http\Response $response
     * @param array $args
     * @return mixed
     */
    public function getAction(Request $request, Response $response, $args)
    {
        return $this->result;
    }

    /**
     * Function to process http POST method
     * @param Slim\Http\Request $request
     * @param Slim\Http\Response $response
     * @param array $args
     * @return mixed
     */
    public function postAction(Request $request, Response $response, $args)
    {
        return $this->result;
    }

    /**
     * Function to process http PUT method
     * @param Slim\Http\Request $request
     * @param Slim\Http\Response $response
     * @param array $args
     * @return mixed
     */
    public function putAction(Request $request, Response $response, $args)
    {
        var_dump("HOLA");
        return $this->result;
    }

    /**
     * Function to process http PATCH method
     * @param Slim\Http\Request $request
     * @param Slim\Http\Response $response
     * @param array $args
     * @return mixed
     */
    public function patchAction(Request $request, Response $response, $args)
    {
        return $this->result;
    }

    /**
     * Function to process http DELETE method
     * @param Slim\Http\Request $request
     * @param Slim\Http\Response $response
     * @param array $args
     * @return mixed
     */
    public function deleteAction(Request $request, Response $response, $args)
    {
        return $this->result;
    }
}
