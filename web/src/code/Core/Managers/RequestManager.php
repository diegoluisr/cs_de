<?php

namespace Core\Managers;

use Slim\Http\Request;
use Slim\Http\Response;
use Utilities\StringUtility;

/**
 * Base classe with default endpoints implementation, this class allow to answer properly
 * any request.
 */
class RequestManager
{

    /**
     * Overwritting postAction.
     * @param string $method
     * @param Slim\Http\Request $request
     * @param Slim\Http\Response $response
     * @param array $args
     */
    public static function processAction($method, Request $request, Response $response, $args)
    {
        $module = StringUtility::slug2CamelCase($args['module']);
        $controller = StringUtility::slug2CamelCase($args['controller']);
        $data = [];
        $class = new \ReflectionClass('Modules\\' . $module . '\\Controllers\\' . $controller . 'Controller');
        $instance = $class->newInstance();
        switch ($method) {
            case 'GET':
                $data = $instance->getAction($request, $response, $args);
                break;
            case 'POST':
                $data = $instance->postAction($request, $response, $args);
                break;
            case 'PUT':
                $data = $instance->putAction($request, $response, $args);
                break;
            case 'PATCH':
                $data = $instance->getAction($request, $response, $args);
                break;
            case 'DELETE':
                $data = $instance->deleteAction($request, $response, $args);
                break;
        }
        $response = $response->withJson($data);
        return $response;
    }
}
