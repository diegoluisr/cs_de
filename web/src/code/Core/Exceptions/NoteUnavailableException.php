<?php

namespace Core\Exceptions;

/**
 * Custom exception to handle unavailable notes.
 */
class NoteUnavailableException extends \Exception
{
    /**
     * Contructor method.
     */
    public function __construct($message, $code = 0, Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

    /**
     * Overwritting toString function.
     */
    public function __toString()
    {
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }
}
