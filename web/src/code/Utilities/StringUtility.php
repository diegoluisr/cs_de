<?php

namespace Utilities;

/**
 * String utilities to transform and create strings.
 */
class StringUtility
{

    /**
     * Funcion que se le entrega una cadena en "slug_format" y lo devuelve en formato "CamelCase"
     *
     * @param string $slug
     * @return string
     */
    public static function slug2CamelCase($slug)
    {
        $slug = str_replace(['-', '_'], ' ', $slug);
        $slug = ucwords($slug);
        $slug = str_replace(' ', '', $slug);
        return $slug;
    }
}
