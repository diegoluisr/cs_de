<?php

use Core\Managers\RequestManager;

$app->group('/api/{module}/{controller}', function () {
    $this->get('/', function ($request, $response, $args) {
        return RequestManager::processAction('GET', $request, $response, $args);
    });

    $this->post('/', function ($request, $response, $args) {
        return RequestManager::processAction('POST', $request, $response, $args);
    });

    $this->put('/', function ($request, $response, $args) {
        return RequestManager::processAction('PUT', $request, $response, $args);
    });

    $this->patch('/', function ($request, $response, $args) {
        var_dump("LOLA");
        return RequestManager::processAction('PATCH', $request, $response, $args);
    });

    $this->delete('/', function ($request, $response, $args) {
        return RequestManager::processAction('DELETE', $request, $response, $args);
    });
});

$app->get('/[{name}]', function (Request $request, Response $response, array $args) {
    return $this->renderer->render($response, 'index.phtml', $args);
});
